import requests
import argparse

# parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument('-s', metavar='IPADDRESS:PORT', type=str, required=True,
                help='the location of the front-end schedular.')
args = ap.parse_args()

requests.post('http://%s' % args.s)