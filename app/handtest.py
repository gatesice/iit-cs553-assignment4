class Program(object):
    programs = {}
    
    @classmethod
    def run(cls, command, args):
        if command in Program.programs:
            try:
                result = Program.programs[command]['function'](args)
                return True, result
            except BaseException as err:
                return False, err.message
        return False, 'Unknown command!'
    
    @classmethod
    def regist(cls, name):
        def regist_func(func):
            Program.programs[name] = {
                'function':func
            }
        return regist_func

@Program.regist('post_tasks_submit')
def post_tasks_submit(a):
    import requests
    from simplejson import dumps as tojson, loads as fromjson
    from app.serializer import STaskSubmission
    
    sts = STaskSubmission()
    data = {
        'count':3,
        'tasks':[
            {'id':'1', 'command':'sleep', 'args':'1000'},
            {'id':'2', 'command':'sleep', 'args':'8000'},
            {'id':'3', 'command':'sleep', 'args':'5000'}
        ]
    }
    
    json = tojson(sts.serialize(data))
    
    res = requests.post(
        'http://localhost:5000/tasks/submit',
        data=json,
        headers={'Content-Type': 'application/json'}
    )

    return res

@Program.regist('post_tasks_submit2')
def post_tasks_submit_expecting_error(a):
    import requests
    from simplejson import dumps as tojson, loads as fromjson
    from app.serializer import STaskSubmission
    
    sts = STaskSubmission()
    data = {
        'count':4,
        'tasks':[
            {'id':'1', 'command':'sleep', 'args':'1000'},
            {'id':'2', 'command':'sleep', 'args':'8000'},
            {'id':'3', 'command':'sleep', 'args':'5000'}
        ]
    }
    
    json = tojson(sts.serialize(data))
    
    res = requests.post(
        'http://localhost:5000/tasks/submit',
        data=json,
        headers={'Content-Type': 'application/json'}
    )

    return res
    
@Program.regist('get_task_results_by_cursor')
def get_task_results_by_cursor(cursor):
    import requests
    from simplejson import dumps as tojson, loads as fromjson
    
    res = requests.get('http://localhost:5000/tasks/results/%s' % cursor)
    
    return fromjson(res.text)

@Program.regist('upload_test')
def upload_test(a):
    from boto.s3.connection import Location, S3Connection
    from config import WorkerConfig as config
    conn = S3Connection(config.AWS_KEY, 
                        config.AWS_SECRET,
                        host="s3-us-west-2.amazonaws.com")
    bucket = conn.get_bucket(config.S3_BUCKET)

    import os
    source_path = '/Users/Gates_ice/Public/1.mp4'
    source_size = os.stat(source_path).st_size    
    
    mp = bucket.initiate_multipart_upload(os.path.basename(source_path))
    chunk_size = 10000000
    import math
    chunk_count = int(math.ceil(source_size / chunk_size))    
    
    from filechunkio import FileChunkIO
    
    for i in range(chunk_count + 1):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(source_path, 'r', offset=offset, bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i+1)
            
    mp.complete_upload()
    
def generate_animoto_workload_file(filename):
    import os
    jpgs = os.listdir(os.path.abspath('jpgs'))
    fp = open('files/%s' % filename, 'w')
    for i in range(0, 14):
        if i > 0:
            fp.write('\n')
        fp.write('animoto')
        for j in range(0, 60):
            index = j + i * 60
            fp.write(' "http://f.yun.moe/%s"' % jpgs[index % len(jpgs)])
            
    fp.close()
    
def generate_sleep_workload_file(filename, time, num):
    import os
    fp = open('files/%s' % filename, 'w')
    for i in range(num):
        if i > 0:
            fp.write('\n')
        fp.write('sleep %d' % time)
    fp.close()
            
@Program.regist('get_all_instances')
def get_all_instances(a):
    from config import SchedulerConfig as config
    import boto.ec2
    conn = boto.ec2.connect_to_region(config.AWS_REGION,
                                      aws_access_key_id=config.AWS_KEY,
                                      aws_secret_access_key=config.AWS_SECRET)
    reservations = conn.get_all_instances()
    for reservation in reservations:
        for instance in reservation.instances:
            print('')
            print('id:         %s'%instance.id)
            print('image_id:   %s'%instance.image_id)
            print('public ip:  %s'%instance.ip_address)
            print('private ip: %s'%instance.private_ip_address)
            
        
if __name__ == '__main__':
    import sys
    if len(sys.argv) > 2:
        Program.run(sys.argv[1], sys.argv[2])
    else:
        Program.run(sys.argv[1], None)