from config import ClientConfig as config
from serializer import (STaskSubmission,
                        STaskSubmissionResult,
                        STaskResultsByCursor)
#from worker import run

import argparse
import requests
import os
import simplejson
import re
from time import sleep, time

cursor = -1

_STaskSubmission = STaskSubmission()
_STaskSubmissionResult = STaskSubmissionResult()
_STaskResultsByCursor = STaskResultsByCursor()

task_pattern = re.compile('(\S*?)\s+(.*)')

def post_tasks(host, tasks):
    """
    Post tasks in a given workload file.
    
    ** parameter **:
    
    ``host``: the host of the Front-end Schedular
    ``tasks``: The tasks to be submitted.
    
    ** return **:
    
    The cursor of the submitted task.
    If any error occurs, return ``None``.
    
    """
    count = len(tasks)
    _d = {
        'count':count,
        'tasks':[]
    }
    for i in range(count):
        # parse command and args
        _command, _args = task_pattern.findall(tasks[i])[0]
        _t = {
            'id':str(i),
            'command':_command,
            'args':_args
        }
        _d['tasks'].append(_t)
    res = requests.post(
        'http://%s/tasks/submit' % host, 
        data=simplejson.dumps(_STaskSubmission.serialize(_d)),
        headers={'Content-Type':'application/json'}
    )
    res_json = _STaskSubmissionResult.deserialize(simplejson.loads(res.text))
    try:
        if res_json['status'] == 'success':
            return res_json['cursor']
    except:
        pass
    return None

def get_task_result(host, cursor):
    """
    Get the result of tasks with given cursor.
    
    ** parameter **:
    
    ``host``: the host of the Front-end Schedular
    
    ``cursor``: The cursor of the submitted task
    
    ** return **:
    
    Return the array of tasks with result.
    If tasks are not done or error occurs, return None.
    """
    res = requests.get('http://%s/tasks/results/%s' % (host, cursor))
    if res.status_code != 200:
        return None
    res_json = _STaskResultsByCursor.deserialize(simplejson.loads(res.text))
    try:
        if res_json['status'] == 'success':
            return res_json['tasks']
    except:
        pass
    return None




def main(host, workload_file):
    tasks = open(workload_file, mode='r').readlines()
    global cursor
    cursor = post_tasks(host, tasks)
    if cursor is None:
        print('Submitting task failed!')
        return
    print('Submit successfully! cursor=%s' % cursor)
    results = None
    t1 = time()
    while results is None:
        results = get_task_result(host, cursor)
        #run('sleep', '2000')
        sleep(2)
    
    
    for result in results:
        #print(result)
        output = """
Task:   %s
Result: %s
%s
        """ % (result['id'], result['result'], 'success' if result['success'] else 'failed')
        print(output)
    print('time used: %.2f seconds.' % (time() - t1))
        

if __name__ == '__main__':
    # parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument('-s', metavar='IPADDRESS:PORT', type=str, required=True,
                    help='the location of the front-end schedular.')
    ap.add_argument('-w', metavar='WORKLOAD_FILE', type=str, required=True,
                    help='the local file that will store the tasks that need to be submitted to the schedular.')
    args = ap.parse_args()

    #     valiedate file
    if not os.path.exists(args.w):
        f = open(args.w, mode='r')
        f.close()
    
    # run the program
    main(args.s, args.w)