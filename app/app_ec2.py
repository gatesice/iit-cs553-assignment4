from aws_services import launch_a_remote_worker_instance
from config import WorkerConfig as config
import sys

try:
    num = int(sys.argv[1])
except:
    num = 1
    
print('launching {} instances'.format(num))
for i in range(num):
    launch_a_remote_worker_instance(config)