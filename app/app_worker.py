# This is the entry point for remote workers.
# This will creates a remote master for workers.
from worker import remotemaster
from config import WorkerConfig as config

from argparse import ArgumentParser

if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('-i', metavar='NUM', type=int, default=3*60,
                    help='The idle time that the worker will stay active.')
    args = ap.parse_args()
    
    if hasattr(args, 'i'):
        config.TIMEOUT = args.i
        
    print('Worker idle time cap = %d sec' % config.TIMEOUT)
    remotemaster.main(args)
    
