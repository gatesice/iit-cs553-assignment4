from config import SchedulerConfig as config
from serializer import (STask, 
                            STaskSubmission, 
                            STaskResult, 
                            STaskResultsByCursor,
                            STaskSubmissionResult,
                            STaskSubmission,
                            STaskResultQuery)
from messages import construct_messages

from flask import Flask, request
from uuid import uuid1
from simplejson import dumps as tojson, loads as fromjson
from threading import Thread
from aws_services import self_termination, stop_all_worker, get_num_workers, launch_a_remote_worker_instance
from time import sleep

app = Flask(__name__)

_STask = STask()
_STaskSubmission = STaskSubmission()
_STaskResult = STaskResult()
_STaskResultByCursor = STaskResultsByCursor()
_STaskSubmissionResult = STaskSubmissionResult()
_STaskSubmission = STaskSubmission()
_StaskResultQuery = STaskResultQuery()

class SQS(object):
    @classmethod
    def init(cls):
        from aws_services import init_queue
        cls.connection, cls.qtasks, cls.qresults = init_queue(config)
        return
        
class DB(object):
    @classmethod
    def init(cls):
        from aws_services import init_database
        cls.connection, cls.ttasks = init_database(config)
        return
    
class ResultHandler(object):
    results = {}
    
    @classmethod
    def new_cursor(cls, cursor, submission):
        """
        return False when cursor exists
        """
        if cursor in cls.results.keys():
            return False
        _c = {'completed':{}, 'details':{}, 'result':{}}
        for t in submission['tasks']: #STask
            _c['completed'][t['id']] = False
            _c['details'][t['id']] = t
            _c['result'][t['id']] = {}
        cls.results[cursor] = _c
        return True
    
    @classmethod
    def completed_cursor(cls, cursor):
        """
        return True if all tasks in a cursor has completed
        """
        return not (False in cls.results[cursor]['completed'].values())
        
    @classmethod
    def push_result(cls, cursor, result):
        """
        push a result in a cursor into the results
        """
        # result is STaskResult
        try:
            _id = result['id']
            cls.results[cursor]['result'][_id] = result
            cls.results[cursor]['completed'][_id] = True
        except KeyError as err:
            return False
        return True
    
    @classmethod
    def get_results(cls, cursor):
        try:
            return [x for x in cls.results[cursor]['result'].values()]
        except:
            return None
        
    @classmethod
    def has_cursor(cls, cursor):
        return cls.results.has_key(cursor)

def start_server():
    app.run(host=config.LISTEN_IP, port=config.__PORT, debug=config.DEBUG)
    return
    
@app.route('/tasks/submit', methods=['POST'])
def submit():
    """
    Submit a group of tasks, return a cursor.
    """
    if request.method =='POST':
        submission = _STaskSubmission.deserialize(request.get_json())
        try:
            assert submission['count'] == len(submission['tasks'])
        except AssertionError as err:
            return tojson({
                'status':'failed',
                'reason':"""The count doesn't match the number of tasks."""
            }), 400
        cursor = str(uuid1())
        while not ResultHandler.new_cursor(cursor, submission):
            cursor = str(uuid1())        
        for m in construct_messages(cursor, submission):
            SQS.qtasks.write(m)
        return tojson(_STaskSubmissionResult.serialize({
            'status':'success',
            'cursor':cursor
        }))
    return 'Method not allowed', 405

@app.route('/tasks/results/<cursor>', methods=['GET'])
def query(cursor):
    """
    Query a cursor, return all results if all the tasks under the cursor is completed.
    """
    if request.method == 'GET':
    #return tojson(ResultHandler.results)
        if not ResultHandler.has_cursor(cursor):
            return tojson(_STaskResultByCursor.serialize({
                'status':'no cursor',
                'cursor':cursor,
                'count':0,
                'tasks':[]
            })), 404
        if not ResultHandler.completed_cursor(cursor):
            return tojson(_STaskResultByCursor.serialize({
                'status':'not completed',
                'cursor':cursor,
                'count':0,
                'tasks':[]
            }))
        results = ResultHandler.get_results(cursor)
        return tojson(_STaskResultByCursor.serialize({
            'status':'success',
            'cursor':cursor,
            'count':len(results),
            'tasks':results
        }))
            
    return 'Method not allowed', 405

@app.route('/terminate/all', methods=['POST'])
def terminate_all():
    if request.method == 'POST':
        stop_all_worker(config)
        self_termination(config)
        return 'OK'
    
@app.route('/terminate/workers', methods=['POST'])
def terminate_workers():
    if request.method == 'POST':
        stop_all_worker(config)
        return 'OK'
    
def provisioning(args):    
    task_delta = [0, 0, 0, 0, 0]
    last_task_count = SQS.qtasks.count()
    while True:
        if SQS.qtasks.count() > 0 and 0 == get_num_workers(config):
            print('[S] Launching first instance for tasks...')
            launch_a_remote_worker_instance(config)
        if args.auto:
            # auto provisioning
            _c = last_task_count
            last_task_count = SQS.qtasks.count()
            task_delta.pop(0)
            task_delta.append(last_task_count - _c)
            print('[S] Dynamic Provisioning ...')
            print(task_delta)
            cond1 = False not in [x > 0 for x in task_delta]
            cond2 = get_num_workers(config) < config.AUTO_PROVISIONING_MAX_WORKERS
            if cond1 and cond2:
                print('[S] Launching new instance...')
                launch_a_remote_worker_instance(config)
        sleep(config.SCAN_INTERVAL)
    return

def main(args):
    """
    Main function of localwork mode of schedular.
    
    ``args`` should be an object from ArgumentParser
    """
    config.__PORT = args.s
    SQS.init()
    DB.init()
    
    flask_proc = Thread(target=start_server)
    flask_proc.start()
    
    if args.auto:
        provisioning_proc = Thread(target=provisioning, args=(args,))
        provisioning_proc.start()

    while True:
        # Get worker's results from SQS
        try:                
            results = SQS.qresults.get_messages(10, message_attributes='.*')
            for result in results:
                attr = result.message_attributes
                text = result.get_body()
                if text == 'Result':
                    print('[Schedular] Got a result from Amazon SQS...')
                    # Reconstruct the result to ResultHandler result type
                    _result = {
                        'success':attr['success']['string_value'],
                        'result':attr['result']['string_value'],
                        'cursor':attr['cursor']['string_value'],
                        'id':attr['id']['string_value']
                    }
                    
                    
                    # Push result into Local Result Handler
                    if ResultHandler.push_result(_result['cursor'], _result):
                        print('[Schedular] The result has been added into ResultHandler')
                    else:
                        print('[Schedular] The result is not pushed into ResultHandler')
                        print(_result)
                        SQS.qresults.delete_message(result)
                    SQS.qresults.delete_message(result)                    
        except BaseException as err:
            print('[Schedular] An error has occured:')
            print(err)
            
    return