from config import SchedulerConfig as config
from serializer import (STask, 
                            STaskSubmission, 
                            STaskResult, 
                            STaskResultsByCursor,
                            STaskSubmissionResult,
                            STaskSubmission,
                            STaskResultQuery)
from messages import construct_local_messages
from worker.localworker import main as local_worker
from timer import IdleTimer

from multiprocessing import Process, Manager
from threading import Thread
from flask import Flask, request
from uuid import uuid1
from simplejson import dumps as tojson, loads as fromjson
from Queue import Empty, Full
import requests

app = Flask(__name__)

_STask = STask()
_STaskSubmission = STaskSubmission()
_STaskResult = STaskResult()
_STaskResultByCursor = STaskResultsByCursor()
_STaskSubmissionResult = STaskSubmissionResult()
_STaskSubmission = STaskSubmission()
_StaskResultQuery = STaskResultQuery()

class MP(object): #Multi-Processing Wrapper   
    @classmethod
    def init(cls, nproc):
        cls.pool = {}
        cls.nproc = nproc
        cls.manager = Manager()
        cls.tasks = cls.manager.Queue()
        cls.results = cls.manager.Queue()
        for i in range(cls.nproc):
            proc = Process(target=local_worker, args=(cls.tasks, cls.results, i))
            cls.pool[i] = proc
        return
        
    @classmethod
    def start_all(cls, recreate=False):
        for pid, proc in cls.pool.items():
            if not proc.is_alive():
                if recreate:
                    cls.pool[pid] = Process(target=local_worker, args=(cls.tasks, cls.results, pid))
                    proc = cls.pool[pid]
                proc.start()
        return
            
    @classmethod
    def join_all(cls):
        for proc in cls.pool.values():
            proc.join()
        return
            
    @classmethod
    def is_any_alive(cls):
        for proc in cls.pool.values():
            if proc.is_alive():
                return True
        return False
    
    @classmethod
    def terminate_all(cls):
        for proc in cls.pool.values():
            if proc.is_alive():
                proc.terminate()
        return
    
class ResultHandler(object):
    results = {}
    
    @classmethod
    def new_cursor(cls, cursor, submission):
        """
        return False when cursor exists
        """
        if cursor in cls.results.keys():
            return False
        _c = {'completed':{}, 'details':{}, 'result':{}}
        for t in submission['tasks']: #STask
            _c['completed'][t['id']] = False
            _c['details'][t['id']] = t
            _c['result'][t['id']] = {}
        cls.results[cursor] = _c
        return True
    
    @classmethod
    def completed_cursor(cls, cursor):
        """
        return True if all tasks in a cursor has completed
        """
        return not (False in cls.results[cursor]['completed'].values())
        
    @classmethod
    def push_result(cls, cursor, result):
        """
        push a result in a cursor into the results
        """
        # result is STaskResult
        try:
            _id = result['id']
            cls.results[cursor]['result'][_id] = result
            cls.results[cursor]['completed'][_id] = True
        except KeyError as err:
            return False
        return True
    
    @classmethod
    def get_results(cls, cursor):
        try:
            return [x for x in cls.results[cursor]['result'].values()]
        except:
            return None
        
    @classmethod
    def has_cursor(cls, cursor):
        return cls.results.has_key(cursor)

# Server
def start_server(port, mp, result_handler, config=config):
    global ResultHandler
    ResultHandler = result_handler
    global MP
    MP = mp
    app.run(host=config.LISTEN_IP, port=port, debug=config.DEBUG)
    return


def local_start_server():
    app.run(host=config.LISTEN_IP, port=config.__PORT, debug=config.DEBUG)
    return
    
@app.route('/tasks/submit', methods=['POST'])
def submit():
    """
    Submit a group of tasks, return a cursor.
    """
    if request.method =='POST':
        submission = _STaskSubmission.deserialize(request.get_json())
        try:
            assert submission['count'] == len(submission['tasks'])
        except AssertionError as err:
            return tojson({
                'status':'failed',
                'reason':"""The count doesn't match the number of tasks."""
            }), 400
        cursor = str(uuid1())
        while not ResultHandler.new_cursor(cursor, submission):
            cursor = str(uuid1())
        for message in construct_local_messages(cursor, submission):
            MP.tasks.put(message)
        return tojson(_STaskSubmissionResult.serialize({
            'status':'success',
            'cursor':cursor
        }))
    return 'Method not allowed', 405

@app.route('/tasks/results/<cursor>', methods=['GET'])
def tasks_result(cursor):
    """
    Get the result of tasks by cursor
    """
    if request.method == 'GET':
        #return tojson(ResultHandler.results)
        if not ResultHandler.has_cursor(cursor):
            return tojson(_STaskResultByCursor.serialize({
                'status':'no cursor',
                'cursor':cursor,
                'count':0,
                'tasks':[]
            })), 404
        if not ResultHandler.completed_cursor(cursor):
            return tojson(_STaskResultByCursor.serialize({
                'status':'not completed',
                'cursor':cursor,
                'count':0,
                'tasks':[]
            }))
        results = ResultHandler.get_results(cursor)
        return tojson(_STaskResultByCursor.serialize({
            'status':'success',
            'cursor':cursor,
            'count':len(results),
            'tasks':results
        }))
            
    return 'Method not allowed', 405

def main(args):
    """
    Main function of localwork mode of schdular.

    ``args`` should be an object from ArgumentParser:
    
    ``args.s`` - the port the serve
    
    ``args.lw`` - number of local workers it starts.
    """
    config.__PORT = args.s
    MP.init(args.lw)
    MP.start_all()
    
    flask_proc = Thread(target=local_start_server)
    flask_proc.start()
    
    #timer = IdleTimer(config.TIMEOUT)
    
    #def _cond():
        #cond1 = MP.is_any_alive()
        ##cond2 = not timer.is_timeout()
        #return cond1
        ##return cond1 and cond2
    
    workers_exited = False
    results = {}
    while True:
        #if MP.tasks.empty():
            #print('Timeout, and no more tasks, Stopping server...')
            #workers_exited = True
        
        if (not workers_exited) and (not MP.is_any_alive()) and (not MP.tasks.empty()):
            MP.start_all(recreate=True)
            
        try:
            result = MP.results.get(False)
            print('[M] Get a result from worker...')
            if not ResultHandler.push_result(result['cursor'], result):
                print('[M] The result is not pushed into ResultHandler.')
            
        except Empty as err:
            if workers_exited: break
    
    return