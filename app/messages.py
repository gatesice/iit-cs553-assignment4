from boto.sqs.message import Message

def construct_messages(cursor, task_submission):
    messages = []
    for i in range(task_submission['count']):
        t = task_submission['tasks'][i]
        
        m = Message()
        m.set_body(u'Task')
        m.message_attributes = {
            'cursor':{
                'data_type': 'String',
                'string_value': cursor
            },
            'id':{
                'data_type': 'Number',
                'string_value': t['id']
            },
            'command':{
                'data_type': 'String',
                'string_value': t['command']
            },
            'args':{
                'data_type': 'String',
                'string_value': t['args']
            }
        }
        
        messages.append(m)
    
    return messages

def construct_local_messages(cursor, task_submission):
    messages = []
    
    for i in range(task_submission['count']):
        t = task_submission['tasks'][i]
        
        m = {'cursor':cursor}
        m.update(t)
        
        messages.append(m)
        
    return messages
    
    