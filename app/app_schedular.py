from schedular import local, remote
from config import SchedulerConfig as config

from argparse import ArgumentParser


if __name__ == '__main__':
    ap = ArgumentParser()
    ap.add_argument('-s', metavar='PORT', type=int, required=True,
                    help='the server port where the clients will connect to submit tasks and to retrieve results.')
    ap.add_argument('-lw', metavar='NUM', type=int,
                    help='number of local workers.')
    ap.add_argument('-rw', action='store_true',
                    help='use remote workers.')
    ap.add_argument('-auto', action='store_true',
                    help='open auto provisioning')
    args = ap.parse_args()
    
    if args.rw:
        remote.main(args)
    elif hasattr(args, 'lw'):
        print('Local workers = %d' % args.lw)
        local.main(args)
    else:
        print('You must specify at least one argument between -lw or -rw.')
        ap.print_help()