import boto
import boto.sqs as sqs
import boto.dynamodb2 as db
import boto.ec2
import boto.s3

from boto.dynamodb2.table import Table
from boto.dynamodb2.fields import HashKey, RangeKey, KeysOnlyIndex, AllIndex
from boto.dynamodb2.types import NUMBER, STRING_SET
from boto.s3.connection import Location, S3Connection

import requests


def init_queue(config):
    """
    Create Amazon SQS connections and return two SQS Queue objects.
    """
    conn = sqs.connect_to_region(config.AWS_REGION,
                                 aws_access_key_id=config.AWS_KEY,
                                 aws_secret_access_key=config.AWS_SECRET)
    task_queue = conn.create_queue(config.QUEUE_TASKS)
    result_queue = conn.create_queue(config.QUEUE_RESULTS)

    return conn, task_queue, result_queue

def init_database(config):
    """
    Create Amazon DynamoDB connection and return table objects.
    """
    conn = db.connect_to_region(config.AWS_REGION,
                                aws_access_key_id=config.AWS_KEY,
                                aws_secret_access_key=config.AWS_SECRET)
    table_tasks = Table(
        config.DB_TABLE_TASKS,
        schema=[
            HashKey('cursor'),
            RangeKey('id'),
        ], throughput={
            'read':2,
            'write':2,
        }, indexes=[
            AllIndex('table_index', parts=[
                HashKey('cursor'),
                RangeKey('id'),
            ])
        ], connection=conn
    )
    
    return conn, table_tasks

def create_table(config):
    conn = db.connect_to_region(config.AWS_REGION,
                                aws_access_key_id=config.AWS_KEY,
                                aws_secret_access_key=config.AWS_SECRET)
    
    table_tasks = Table.create(
        config.DB_TABLE_TASKS,
        schema=[
            HashKey('cursor'),
            RangeKey('id'),
        ], throughput={
            'read':2,
            'write':2,
        }, indexes=[
            AllIndex('table_index', parts=[
                HashKey('cursor'),
                RangeKey('id'),
            ])
        ], connection=conn
    )
    
def self_termination(config):
    try:
        res = requests.get('http://169.254.169.254/latest/meta-data/instance-id')
        conn = boto.ec2.connect_to_region(config.AWS_REGION,
                                          aws_access_key_id=config.AWS_KEY,
                                          aws_secret_access_key=config.AWS_SECRET)
        conn.terminate_instances(instance_ids=[res.text])
    except BaseException as err:
        return err
    return None # In fact if success, it should not return this

def s3_upload(config, filename):
    conn = S3Connection(config.AWS_KEY, 
                        config.AWS_SECRET,
                        host="s3-us-west-2.amazonaws.com")
    bucket = conn.get_bucket(config.S3_BUCKET)

    import os
    source_path = filename
    source_size = os.stat(source_path).st_size   
    if source_size == 0:
        raise BaseException('The size of the file is 0!')
    
    mp = bucket.initiate_multipart_upload(os.path.basename(source_path))
    chunk_size = 10000000
    import math
    chunk_count = int(math.ceil(source_size / chunk_size))    
    
    from filechunkio import FileChunkIO
    
    for i in range(chunk_count + 1):
        offset = chunk_size * i
        bytes = min(chunk_size, source_size - offset)
        with FileChunkIO(source_path, 'r', offset=offset, bytes=bytes) as fp:
            mp.upload_part_from_file(fp, part_num=i+1)
            
    mp.complete_upload()
    file_key = bucket.get_key(mp.key_name)
    return file_key.generate_url(300, force_http=True)
    
    
def launch_a_remote_worker_instance(config):
    conn = boto.ec2.connect_to_region(config.AWS_REGION,
                                      aws_access_key_id=config.AWS_KEY,
                                      aws_secret_access_key=config.AWS_SECRET)
    spot_request = conn.request_spot_instances(config.EC2_SPOT_PRICE,
                                               config.EC2_WORKER_IMAGE_ID,
                                               security_group_ids=[config.EC2_SECURITY_GROUP_ID],
                                               key_name=config.EC2_KEY_PAIR,
                                               instance_type=config.EC2_INSTANCE_TYPE)
    
    return spot_request

def stop_all_worker(config):
    from config import SchedulerConfig as config
    import boto.ec2
    conn = boto.ec2.connect_to_region(config.AWS_REGION,
                                      aws_access_key_id=config.AWS_KEY,
                                      aws_secret_access_key=config.AWS_SECRET)
    reservations = conn.get_all_instances()
    for reservation in reservations:
        for instance in reservation.instances:
            if instance.image_id == config.EC2_WORKER_IMAGE_ID:
                conn.terminate_instances(instance.id)
                print('%s has stopped.'%(instance.id))
    return True
    
def get_num_workers(config, running=True):
    from config import SchedulerConfig as config
    import boto.ec2
    conn = boto.ec2.connect_to_region(config.AWS_REGION,
                                      aws_access_key_id=config.AWS_KEY,
                                      aws_secret_access_key=config.AWS_SECRET)
    reservations = conn.get_all_instances()
    nworker = 0
    for reservation in reservations:
        for instance in reservation.instances:
            if instance.image_id == config.EC2_WORKER_IMAGE_ID:
                if instance.state_code == 16:
                    nworker += 1
                elif running == False or instance.state_code == 0:
                    nworker += 1
    spot_requests = conn.get_all_spot_instance_requests()
    for spot_request in spot_requests:
        if spot_request.state == 'open':
            nworker += 1
    return nworker