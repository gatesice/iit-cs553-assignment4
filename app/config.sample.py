# CONFIG FILE SAMPLE
# CREATE A COPY OF THIS FILE AND RENAME THIS FILE TO config.py

class Config():
    AWS_REGION = 'us-west-2'
    
    QUEUE_TASKS = 'cs553hw4tasks'
    QUEUE_RESULTS = 'cs553hw4results'
    
    DB_TABLE_TASKS = 'cs553hw4tasks'
    DB_TABLE_MSGS = 'cs553hw4msgs'
    DB_TABLE_RESULTS = 'cs553hw4results'   
    
    EC2_SECURITY_GROUP_ID = 'sg-d61c79b3'
    EC2_KEY_PAIR = 'cs553hw4'
    EC2_SPOT_PRICE = '0.1'
    EC2_INSTANCE_TYPE = 'c3.large'
    EC2_WORKER_IMAGE_ID = 'ami-03c09733'
    
    S3_BUCKET = 'cs553hw4'
    
    TIMEOUT = 15 * 60
    NUMBER_OF_WORKER_PROCESS = 16    
    SCAN_INTERVAL = 10
    
    AUTO_PROVISIONING_MAX_WORKERS = 4
class ClientConfig(Config):
    AWS_KEY = 'Your AWS Key Here'
    AWS_SECRET = 'Your AWS Secret Here'

class SchedulerConfig(Config):
    LISTEN_IP = '0.0.0.0'
    PORT = 10000
    DEBUG = False
    
    AWS_KEY = 'Your AWS Key Here'
    AWS_SECRET = 'Your AWS Secret Here'    
    SCAN_INTERVAL = 10
    
class MonitorConfig(Config):
    AWS_KEY = 'Your AWS Key Here'
    AWS_SECRET = 'Your AWS Secret Here'
    
class WorkerConfig(Config):
    AWS_KEY = 'Your AWS Key Here'
    AWS_SECRET = 'Your AWS Secret Here'
    
    TIMEOUT = 3 * 60
    
    QUEUE_MESSAGES_PER_GET = 10
    
class MonitorConfig(Config):
    pass