from config import WorkerConfig as config

from worker.remoteworker import main as remote_worker
from multiprocessing import Process
from Queue import Empty, Full
from timer import IdleTimer
from aws_services import init_queue, self_termination
from time import sleep

class MP(object): #Multi-Processing Wrapper   
    @classmethod
    def init(cls, nproc):
        cls.pool = {}
        cls.nproc = nproc
        for i in range(cls.nproc):
            proc = Process(target=remote_worker, args=(i,))
            cls.pool[i] = proc
        return
        
    @classmethod
    def start_all(cls, recreate=False):
        for pid, proc in cls.pool.items():
            if not proc.is_alive():
                if recreate:
                    cls.pool[pid] = Process(target=remote_worker, args=(pid))
                    proc = cls.pool[pid]
                proc.start()
        return
            
    @classmethod
    def join_all(cls):
        for proc in cls.pool.values():
            proc.join()
        return
            
    @classmethod
    def is_any_alive(cls):
        for proc in cls.pool.values():
            if proc.is_alive():
                return True
        return False
    
    @classmethod
    def terminate_all(cls):
        for proc in cls.pool.values():
            if proc.is_alive():
                proc.terminate()
        return
    

def main(args):
    MP.init(config.NUMBER_OF_WORKER_PROCESS)
    MP.start_all()
    
    timer = IdleTimer(config.TIMEOUT)
    sqs_conn, q_tasks, _ = init_queue(config)
    
    while MP.is_any_alive():
        sleep(config.SCAN_INTERVAL)
        
    print('[M] All workers quited, terminating...')
    self_termination(config)
    
    
    