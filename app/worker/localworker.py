from . import __run__ as run
from timer import IdleTimer
from config import WorkerConfig as config
from Queue import Empty, Full
from serializer import STaskResult

_STaskResult = STaskResult()

def main(tasks, results, pid):
    timer = IdleTimer(config.TIMEOUT)
    
    while not timer.is_timeout():
        try:
            print('[%d] Getting message..' % pid)
            msg = tasks.get(True, config.TIMEOUT)   # will wait until get a message
            timer.reset()
        except Empty as err:
            break
        
        print('[%d] Running %s' % (pid, msg['command']))
        success, result = run(msg['command'], msg['args'])
        print('[%d] Run complete..' % pid)
        task_result = {
            'success':success,
            'result':result if result is not None else '',
            'cursor':msg['cursor'],
            'id':msg['id']
        }
        
        try:
            results.put(_STaskResult.serialize(task_result), True, config.TIMEOUT)  # will wait until put into the queue
        except Full as err:
            break