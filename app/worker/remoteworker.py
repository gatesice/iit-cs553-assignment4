from . import __run__ as run

from config import WorkerConfig as config

from timer import IdleTimer
from serializer import STaskResult
from aws_services import init_database, init_queue
from boto.sqs.message import Message

_STaskResult = STaskResult()

def main(pid):
    timer = IdleTimer(config.TIMEOUT)
    
    sqs_conn, q_tasks, q_results = init_queue(config)
    db_conn, tb_tasks = init_database(config)
    
    print('[%d] Worker started...' % pid)
    
    while not timer.is_timeout():
        try:
            msgs = q_tasks.get_messages(config.QUEUE_MESSAGES_PER_GET, message_attributes='.*')
            for msg in msgs:
                timer.reset()
                print('[%d] Got message..' % pid)
                
                # extract items in queue
                cursor = msg.message_attributes['cursor']['string_value']
                id = msg.message_attributes['id']['string_value']
                command = msg.message_attributes['command']['string_value']
                args = msg.message_attributes['args']['string_value']
                
                # check in DynamoDB if it is in the table
                if tb_tasks.has_item(cursor=cursor, id=id):
                    print('[%d] The task exists, skip this' % pid)
                    q_tasks.delete_message(msg)
                    continue
                
                # run the task
                tb_tasks.put_item(data={'cursor':cursor, 'id':id})
                q_tasks.delete_message(msg)
                print('[%d] Running %s' % (pid, command))
                success, result = run(command, args)
                print('[%d] Run complete..' % pid)
                task_result = {
                    'success':success,
                    'result':result if result is not None else '',
                    'cursor':cursor,
                    'id':id
                }
                
                # write the task result into the q_results
                try:
                    msg_result = Message()
                    msg_result.set_body('Result')
                    msg_result.message_attributes = {
                        'success':{
                            'data_type':'String',
                            'string_value':'True' if success else 'False'
                        },
                        'result':{
                            'data_type':'String',
                            'string_value':result if result else ''
                        },
                        'cursor':{
                            'data_type':'String',
                            'string_value':cursor
                        },
                        'id':{
                            'data_type':'String',
                            'string_value':id
                        }
                    }
                    q_results.write(msg_result)
                except BaseException as err:
                    print('[%d] Error writing results...' % pid)
                    print(err)
        except BaseException as err:
            print('[%d] Runtime Error...' % pid)
            print(err)
    
    print('[%d] Worker idle timeout, exiting...' % pid)