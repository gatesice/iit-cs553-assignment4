import os, sys
sys.path.append(
    os.path.abspath(
        os.path.join(__file__, '..')
    )
)
#print(sys.path)

class Program(object):
    programs = {}
    
    @classmethod
    def run(cls, command, args):
        if command in Program.programs:
            try:
                result = Program.programs[command]['function'](args)
                return True, result
            except BaseException as err:
                return False, err.message
        return False, 'Unknown command!'
    
    @classmethod
    def regist(cls, name):
        def regist_func(func):
            Program.programs[name] = {
                'function':func
            }
        return regist_func
    
__run__ = Program.run
__regist__ = Program.regist
            
            
            
@Program.regist('sleep')
def sleep(args):
    duration = int(args)
    
    from time import sleep as _sleep
    
    print('sleeping for %f ms' % duration)    
    _sleep(float(duration) / 1000)
    return 'sleeped %.2f ms' % duration


"""
Animoto:
command: animoto
args: list of url splitted by ' '(space). All spaces in url should be encoded to %20.
result: a link to the encoded video file in S3.
"""
@Program.regist('animoto')
def animoto(args):
    from subprocess import call
    import os, sys
    from uuid import uuid1
    from config import WorkerConfig as config
    
    def create_tmp_dir():
        uuid = str(uuid1())
        tmp_prefix = '/tmp/%s' % uuid
        while os.path.exists(tmp_prefix):
            uuid = str(uuid1())
            tmp_prefix = '/tmp/%s' % uuid
        os.mkdir(tmp_prefix)
        return tmp_prefix, uuid
    
    image_urls = args.split(' ')
    tmp_prefix, uuid = create_tmp_dir()
    
    if len(image_urls) > 999:
        raise(BaseException('Too many urls')) 
    
    # download all image files
    for index in range(len(image_urls)):
        if image_urls[index][0] == image_urls[index][-1] == '"':
            pass
        else:
            image_urls[index] = '"%s"' % image_urls[index]
        #call(['wget', image_urls[index], '-O', ('%s/img%s.jpg' % (tmp_prefix, str(index).zfill(3)))])
        call("wget %s -O %s/img%s.jpg" % (image_urls[index], tmp_prefix, str(index).zfill(3)), shell=True)
    
    # create animoto
    #call(['ffmpeg', '-f', 'image2', '-r', '1', '-i', '{}/img%03d.jpg'.format(tmp_prefix), '{}.mpg'.format(tmp_prefix)])
    call("ffmpeg -f image2 -r 1 -i {}/img%03d.jpg -y -r 25 {}.mpg".format(tmp_prefix, tmp_prefix), shell=True)
    
    # upload video to S3
    from aws_services import s3_upload
    link = s3_upload(config, '{}.mpg'.format(tmp_prefix))
    
    return link