from colander import *

class STask(MappingSchema):
    id = SchemaNode(String())
    command = SchemaNode(String())
    args = SchemaNode(String())
    
class STasks(SequenceSchema):
    task = STask()
    
class STaskSubmission(MappingSchema):
    count = SchemaNode(Int())
    tasks = STasks()

class STaskResult(MappingSchema):
    success = SchemaNode(Bool())
    result = SchemaNode(String())
    cursor = SchemaNode(String())
    id = SchemaNode(String())
    
class STaskResults(SequenceSchema):
    tasks = STaskResult()
    
class STaskResultsByCursor(MappingSchema):
    status = SchemaNode(String())
    cursor = SchemaNode(String())
    count = SchemaNode(Int(), missing=0)
    tasks = STaskResults(missing=[])
   
class STaskSubmissionResult(MappingSchema):
    status = SchemaNode(String())
    cursor = SchemaNode(String())
    
class STaskResultQuery(MappingSchema):
    cursor = SchemaNode(String())
