from time import time

class IdleTimer(object):
    
    def __init__(self, timeout_in_seconds):
        self.timeout = timeout_in_seconds
        self.tbegin = time()
        
    def reset(self):
        self.tbegin = time()
        
    def is_timeout(self):
        return time() - self.tbegin > self.timeout